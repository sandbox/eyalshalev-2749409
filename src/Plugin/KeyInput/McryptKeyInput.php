<?php


namespace Drupal\mcrypt\Plugin\KeyInput;


use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyInput\TextFieldKeyInput;

/**
 * Defines a key input that provides a simple text field.
 *
 * @KeyInput(
 *   id = "mcrypt",
 *   label = @Translation("Mcrypt"),
 *   description = @Translation("The input required by the mcrypt key type.")
 * )
 */
class McryptKeyInput extends TextFieldKeyInput {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'iv_value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['iv_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IV value'),
      '#default_value' => $this->getIv(),
      // Tell the browser not to autocomplete this field.
      '#attributes' => ['autocomplete' => 'off'],
      '#placeholder' => $this->t('The hex value of the input vector')
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  public function getIv() {
    return $this->getConfiguration()['iv_value'];
  }

}