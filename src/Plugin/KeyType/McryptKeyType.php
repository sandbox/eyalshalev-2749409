<?php


namespace Drupal\mcrypt\Plugin\KeyType;


use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyTypeBase;

/**
 * @KeyType(
 *   id = "mcrypt",
 *   label = @Translation("Mcrypt encryption"),
 *   description = @Translation("Used for encrypting and decrypting data with the mcrypt extension."),
 *   group = "encryption",
 *   key_value = {
 *     "plugin" = "mcrypt"
 *   }
 * )
 */
class McryptKeyType extends KeyTypeBase implements KeyPluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'cipher' => NULL,
      'mode' => NULL
    ];
  }

  public function getCipher() {
    return $this->getConfiguration()['cipher'];
  }

  public function getMode() {
    return $this->getConfiguration()['mode'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['cipher'] = [
      '#type' => 'select',
      '#title' => $this->t('Cipher'),
      '#description' => $this->t('The cipher type to use.'),
      '#options' => array_combine(mcrypt_list_algorithms(), mcrypt_list_algorithms())
    ];

    $form['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('The encryption mode to use.'),
      '#options' => array_combine(mcrypt_list_modes(), mcrypt_list_modes())
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }


  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(array $form, FormStateInterface $form_state, $key_value) {
    $require_size = mcrypt_get_key_size($this->getCipher(), $this->getMode());
    if (strlen($key_value) !== $require_size) {
      $form_state->setErrorByName('cipher', $this->t('The size of the encryption key (%actual) does not match the required size (%required) for the selected cipher/mode combination.', [
        '%actual' => strlen($key_value),
        '%required' => $require_size
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    // This feature is not supported.
    return NULL;
  }

}