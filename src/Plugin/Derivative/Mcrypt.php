<?php


namespace Drupal\mcrypt\Plugin\Derivative;


use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Retrieves encryption method definitions for all the mcrypt cipher/mode
 * combinations.
 */
class Mcrypt extends DeriverBase {

  use StringTranslationTrait;

  const TITLE = '@cipher (@mode)';

  const DESCRIPTION = 'Encrypts (& decrypts) using the @cipher cipher and the @mode mode.';

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $ciphers = $this->getMcryptCiphers();
    $modes = $this->getMcryptModes();
    foreach ($ciphers as $cipher) {
      foreach ($modes as $mode) {
        $name = strtolower($cipher . '_' . $mode);
        $this->derivatives[$name] = $base_plugin_definition;
        $this->derivatives[$name]['title'] = $this->t(static::TITLE, [
          '@cipher' => $cipher,
          '@mode' => $mode
        ]);
        $this->derivatives[$name]['description'] = $this->t(static::DESCRIPTION, [
          '@cipher' => $cipher,
          '@mode' => $mode
        ]);
        $this->derivatives[$name]['cipher'] = $cipher;
        $this->derivatives[$name]['mode'] = $mode;
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

  /**
   * A list of all the mcrypt ciphers that are available in the server mcrypt
   * version.
   *
   * @return string[]
   */
  protected function getMcryptCiphers() {
    return array_map('constant', array_filter($this->getMcryptCipherNames(), 'defined'));
  }

  /**
   * A list of all the mcrypt modes.
   * @return string[]
   */
  protected function getMcryptModes() {
    return [
      MCRYPT_MODE_CBC,
      MCRYPT_MODE_CFB,
      MCRYPT_MODE_ECB,
      MCRYPT_MODE_NOFB,
      MCRYPT_MODE_OFB,
      MCRYPT_MODE_STREAM,
    ];
  }

  /**
   * All the possible mcrypt cipher definition names.
   * @link http://php.net/manual/en/mcrypt.ciphers.php
   * @see \Drupal\mcrypt\Plugin\Derivative\Mcrypt::getMcryptCipher()
   * @return string[]
   */
  protected function getMcryptCipherNames() {
    return [
      'MCRYPT_3DES',
      'MCRYPT_ARCFOUR_IV', // (libmcrypt > 2.4.x only)
      'MCRYPT_ARCFOUR', // (libmcrypt > 2.4.x only)
      'MCRYPT_BLOWFISH',
      'MCRYPT_BLOWFISH_COMPAT',
      'MCRYPT_CAST_128',
      'MCRYPT_CAST_256',
      'MCRYPT_CRYPT',
      'MCRYPT_DES',
      'MCRYPT_DES_COMPAT', // (libmcrypt 2.2.x only)
      'MCRYPT_ENIGNA', // (libmcrypt > 2.4.x only, alias for MCRYPT_CRYPT)
      'MCRYPT_GOST',
      'MCRYPT_IDEA', // (non-free)
      'MCRYPT_LOKI97', // (libmcrypt > 2.4.x only)
      'MCRYPT_MARS', // (libmcrypt > 2.4.x only, non-free)
      'MCRYPT_PANAMA', // (libmcrypt > 2.4.x only)
      'MCRYPT_RIJNDAEL_128', // (libmcrypt > 2.4.x only)
      'MCRYPT_RIJNDAEL_192', // (libmcrypt > 2.4.x only)
      'MCRYPT_RIJNDAEL_256', // (libmcrypt > 2.4.x only)
      'MCRYPT_RC2',
      'MCRYPT_RC4', // (libmcrypt 2.2.x only)
      'MCRYPT_RC6_128', // (libmcrypt 2.2.x only)
      'MCRYPT_RC6_192', // (libmcrypt 2.2.x only)
      'MCRYPT_RC6_256', // (libmcrypt 2.2.x only)
      'MCRYPT_RC6', // (libmcrypt > 2.4.x only)
      'MCRYPT_SAFER64',
      'MCRYPT_SAFER128',
      'MCRYPT_SAFERPLUS', // (libmcrypt > 2.4.x only)
      'MCRYPT_SERPENT', // (libmcrypt > 2.4.x only)
      'MCRYPT_SERPENT_128', // (libmcrypt 2.2.x only)
      'MCRYPT_SERPENT_192', // (libmcrypt 2.2.x only)
      'MCRYPT_SERPENT_256', // (libmcrypt 2.2.x only)
      'MCRYPT_SKIPJACK', // (libmcrypt > 2.4.x only)
      'MCRYPT_TEAN', // (libmcrypt 2.2.x only)
      'MCRYPT_THREEWAY',
      'MCRYPT_TRIPLEDES', // (libmcrypt > 2.4.x only)
      'MCRYPT_TWOFISH', // (for older mcrypt 2.x versions, or mcrypt > 2.4.x )
      'MCRYPT_TWOFISH128', // (libmcrypt 2.2.x only)
      'MCRYPT_TWOFISH192', // (libmcrypt 2.2.x only)
      'MCRYPT_TWOFISH256', // (libmcrypt 2.2.x only)
      'MCRYPT_WAKE', // (libmcrypt > 2.4.x only)
      'MCRYPT_XTEA' // (libmcrypt > 2.4.x only)
    ];
  }

}