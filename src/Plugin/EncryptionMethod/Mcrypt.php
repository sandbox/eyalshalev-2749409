<?php


namespace Drupal\mcrypt\Plugin\EncryptionMethod;


use Drupal\encrypt\Plugin\EncryptionMethod\EncryptionMethodBase;
use Drupal\key\KeyInterface;
use Drupal\mcrypt\Plugin\KeyInput\McryptKeyInput;
use Drupal\mcrypt\Plugin\KeyType\McryptKeyType;

/**
 * @EncryptionMethod(
 *   id = "mcrypt",
 *   title = @Translation("Mcrypt encryption"),
 *   description = @Translation("Encrypts (& decrypts) using the mcrypt extension."),
 *   key_type = {
 *     "mcrypt"
 *   }
 * )
 */
class Mcrypt extends EncryptionMethodBase {

  /**
   * {@inheritdoc}
   */
  public function encrypt($text, KeyInterface $key) {
    /** @var \Drupal\mcrypt\Plugin\KeyType\McryptKeyType $key_type */
    $key_type = $key->getKeyType();
    /** @var \Drupal\mcrypt\Plugin\KeyInput\McryptKeyInput $key_input */
    $key_input = $key->getKeyInput();

    return mcrypt_encrypt($key_type->getCipher(), $key->getKeyValue(), $text, $key_type->getMode(), hex2bin($key_input->getIv()));
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($text, KeyInterface $key) {
    /** @var \Drupal\mcrypt\Plugin\KeyType\McryptKeyType $key_type */
    $key_type = $key->getKeyType();
    /** @var \Drupal\mcrypt\Plugin\KeyInput\McryptKeyInput $key_input */
    $key_input = $key->getKeyInput();

    return mcrypt_decrypt($key_type->getCipher(), $key->getKeyValue(), $text, $key_type->getMode(), hex2bin($key_input->getIv()));
  }

  /**
   * {@inheritdoc}
   */
  public function checkDependencies($text = NULL, KeyInterface $key = NULL) {
    $errors = [];
    $key_type = $key->getKeyType();
    $key_input = $key->getKeyInput();

    if (!($key_type instanceof McryptKeyType)) {
      $errors[] = $this->t('The key type class must extend the @class', [
        '@class' => McryptKeyType::class
      ]);
    }

    if (!($key_input instanceof McryptKeyInput)) {
      $errors[] = $this->t('The key input class must extend the @class', [
        '@class' => McryptKeyInput::class
      ]);
    }

    return $errors;
  }

}